public class bai1 {

    public static void main(String[] args) {
        int[] array = {15, 2, 1, 2, 15}; 

        if (isSymmetricArray(array)) {
            System.out.println("Mảng là mảng đối xứng.");
        } else {
            System.out.println("Mảng không phải là mảng đối xứng.");
        }
    }

    public static boolean isSymmetricArray(int[] array) {
        int length = array.length;
        for (int i = 0; i < length / 2; i++) {
            if (array[i] != array[length - 1 - i]) {
                return false;
            }
        }
        return true;
    }
}
