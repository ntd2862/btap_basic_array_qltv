import java.util.Scanner;

public class bai10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Nhập kích thước ma trận n: ");
        int n = scanner.nextInt();
        
        int[][] A = new int[n][n];
        
        System.out.println("Nhập các phần tử của ma trận A:");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print("A[" + i + "][" + j + "]: ");
                A[i][j] = scanner.nextInt();
            }
        }
        
        int sumMainDiagonal = 0;
        int sumSecondaryDiagonal = 0;
        
        
        for (int i = 0; i < n; i++) {
            sumMainDiagonal += A[i][i];
            sumSecondaryDiagonal += A[i][n - i - 1];
        }
        
        System.out.println("Tổng các phần tử trên đường chéo chính: " + sumMainDiagonal);
        System.out.println("Tổng các phần tử trên đường chéo phụ: " + sumSecondaryDiagonal);
    }
}
