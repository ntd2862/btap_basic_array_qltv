public class bai2 {
    public static void main(String[] args) {
        int[] a = {5, 2, 7, 1, 9}; 
        int n = a.length;
        
       
        sapXepTangDan(a, n);
        
        
        System.out.println("Mang sau khi sap xep:");
        hienThiMang(a, n);
    }
    
    public static void sapXepTangDan(int[] a, int n) {
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                if (a[i] > a[j]) {
                   
                    int temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                }
            }
        }
    }
    
    
    public static void hienThiMang(int[] a, int n) {
        for (int i = 0; i < n; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println();
    }
}
