public class bai3 {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 2, 3, 1, 5, 2, 4, 1, 3}; 
        countOccurrences(arr);
    }
    
    public static void countOccurrences(int[] arr) {
        int n = arr.length;
        int maxCount = 0;
        int mostFrequentElement = 0;

        for (int i = 0; i < n; i++) {
            int count = 1;
            if (arr[i] != -1) {
                for (int j = i + 1; j < n; j++) {
                    if (arr[i] == arr[j]) {
                        count++;
                        arr[j] = -1; 
                    }
                }
                if (count > maxCount) {
                    maxCount = count;
                    mostFrequentElement = arr[i];
                }
                System.out.println("Phần tử " + arr[i] + " xuất hiện " + count + " lần.");
            }
        }
        System.out.println("Phần tử xuất hiện nhiều nhất trong mảng là: " + mostFrequentElement);
    }
}