import java.util.Scanner;

public class bai4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        
        System.out.print("Nhập số phần tử của mảng: ");
        int n = scanner.nextInt();
        int[] a = new int[n];
        System.out.println("Nhập các phần tử của mảng:");
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        System.out.print("Nhập số X: ");
        int x = scanner.nextInt();

       
        int closestIndex = findClosestPrimeIndex(a, n, x);

        if (closestIndex != -1) {
            System.out.println("Vị trí của số nguyên tố gần X nhất trong mảng là: " + closestIndex);
        } else {
            System.out.println("Không có số nguyên tố trong mảng.");
        }
    }

    
    private static int findClosestPrimeIndex(int[] a, int n, int x) {
        int minDiff = Integer.MAX_VALUE;
        int closestIndex = -1;

        for (int i = 0; i < n; i++) {
            if (isPrime(a[i])) {
                int diff = Math.abs(a[i] - x);
                if (diff < minDiff) {
                    minDiff = diff;
                    closestIndex = i;
                }
            }
        }

        return closestIndex;
    }

   
    private static boolean isPrime(int num) {
        if (num <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }
}