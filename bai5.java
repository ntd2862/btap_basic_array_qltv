public class bai5 {
    public static void main(String[] args) {
        int[] a = {5, 3, 6, 7};
        int[] b = {2, 9, 11};
        int p = 1;

        int[] result = insertArray(a, b, p);

        System.out.print("(a, " + result.length + "): ");
        for (int value : result) {
            System.out.print(value + " ");
        }
    }

    public static int[] insertArray(int[] a, int[] b, int p) {
        int[] result = new int[a.length + b.length];
        
       
        for (int i = 0; i < p; i++) {
            result[i] = a[i];
        }

        
        for (int i = 0; i < b.length; i++) {
            result[p + i] = b[i];
        }

        
        for (int i = p; i < a.length; i++) {
            result[b.length + i] = a[i];
        }

        return result;
    }
}