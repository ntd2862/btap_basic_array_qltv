import java.util.Scanner;

public class bai6 {
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Nhập số phần tử của mảng: ");
        int n = scanner.nextInt();
        
        int[] a = new int[n];
        
        System.out.println("Nhập các phần tử của mảng:");
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        
        System.out.print("Nhập giá trị cần chèn (x): ");
        int x = scanner.nextInt();
        
     
        insertionSort(a);
        
     
        insertValue(a, x);
        
        System.out.println("Mảng sau khi chèn giá trị x:");
        for (int num : a) {
            System.out.print(num + " ");
        }
        
        scanner.close();
    }
    
   
    public static void insertionSort(int[] arr) {
        int n = arr.length;
        for (int i = 1; i < n; ++i) {
            int key = arr[i];
            int j = i - 1;
            
            
            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = key;
        }
    }
    
   
    public static void insertValue(int[] arr, int x) {
        int n = arr.length;
        int i = n - 1;
        
        
        while (i >= 0 && arr[i] > x) {
            arr[i + 1] = arr[i];
            i--;
        }
        
        
        arr[i + 1] = x;
    }
}
