public class bai7 {

    public static void main(String[] args) {
        int[] arr = {1, 4, 2, 3, 1, 2, 6, 8, 3, 5, 7};
        findLongestIncreasingSubarray(arr);
    }

    public static void findLongestIncreasingSubarray(int[] arr) {
        int maxLength = 1; 
        int startIndex = 0; 
        int currentLength = 1; 
        int currentStart = 0; 

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] >= arr[i - 1]) {
                currentLength++;
                if (currentLength > maxLength) {
                    maxLength = currentLength;
                    startIndex = currentStart;
                }
            } else {
                currentLength = 1;
                currentStart = i;
            }
        }

        System.out.println("Đường chạy dài nhất ở vị trí " + (startIndex + 1) + " với độ dài là " + maxLength);
    }
}
