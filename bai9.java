import java.util.Scanner;

public class bai9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        
        System.out.print("Nhập số hàng của ma trận: ");
        int n = scanner.nextInt();
        System.out.print("Nhập số cột của ma trận: ");
        int m = scanner.nextInt();

        
        int[][] matrix = new int[n][m];

        
        System.out.println("Nhập các phần tử của ma trận:");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                System.out.printf("Nhập phần tử matrix[%d][%d]: ", i, j);
                matrix[i][j] = scanner.nextInt();
            }
        }

        
        System.out.println("Các phần tử của ma trận theo thứ tự tăng dần:");
        int[] sortedArray = flattenAndSort(matrix);
        for (int element : sortedArray) {
            System.out.print(element + " ");
        }
    }

    
    private static int[] flattenAndSort(int[][] matrix) {
        int rowCount = matrix.length;
        int colCount = matrix[0].length;
        int totalElements = rowCount * colCount;
        int[] flattenedArray = new int[totalElements];

        
        int index = 0;
        for (int i = 0; i < rowCount; i++) {
            for (int j = 0; j < colCount; j++) {
                flattenedArray[index++] = matrix[i][j];
            }
        }

        
        for (int i = 0; i < totalElements - 1; i++) {
            for (int j = 0; j < totalElements - i - 1; j++) {
                if (flattenedArray[j] > flattenedArray[j + 1]) {
                    
                    int temp = flattenedArray[j];
                    flattenedArray[j] = flattenedArray[j + 1];
                    flattenedArray[j + 1] = temp;
                }
            }
        }

        return flattenedArray;
    }
}
